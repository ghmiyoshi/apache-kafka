# Apache Kafka

Alura curso de Kafka: Introdução a streams em microsserviços.

## Comandos:

#### Executar servidor
* bin/zookeeper-server-start.sh config/zookeeper.properties
* bin/kafka-server-start.sh config/server.properties

#### Visualizar mensagens
* bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic ECOMMERCE_NEW_ORDER --from-beginning

#### Editar número de partições
* vi config/server.properties
* OBS: O número de partições não será refletido nos tópicos já criados, vale somente para os novos.

#### Descrição dos tópicos
* bin/kafka-topics.sh --bootstrap-server localhost:9092 --describe

#### Alterar número de partições de um tópico
* bin/kafka-topics.sh --alter --zookeeper localhost:2181 --topic ECOMMERCE_NEW_ORDER --partitions 3

#### Apresentar informações dos tópicos
* bin/kafka-topics.sh --bootstrap-server localhost:9092 --describe

#### Apresentar informações dos grupos consumidores 
* bin/kafka-consumer-groups.sh --all-groups --bootstrap-server localhost:9092 --describe

#### Alterar diretórios do Kafka e Zookeeper
* vi config/server.properties 
* vi config/zookeeper.properties

## Conclusão:
Terminei o projeto com diversos micro serviços, pequenos, onde cada um pode ter as suas próprias dependências no seu próprio pom. Então, se algum deles precisar de acesso a banco, a um banco local específico, se precisa de acesso ao cloud para fazer alguma outra coisa, se um terceiro precisar fazer push notification para o celular, sem problemas.

Criei três serviços que ficam escutando, o e-mail, o fraud detector e o log e usei o service-new-order para enviar 10 mensagens. O log loga essas mensagens e o fraud detector analisa essas mensagens e verifica se tem fraude ou não de forma ficticia. O new order também envia um e-mail: “A sua compra está sendo processada”, que vai ser enviado e o service e-mail escuta.

Tenho o service-new-order que envia várias mensagens e vários grupos de consumo escutando dois subjects distintos, ECOMMERCE_NEW_ORDER e ECOMMERCE_SEND_EMAIL. Posso ter também dois fraud detectors rodando, dentro de um consumer group ou cinco services e-mail rodando dentro de um consumer group ou quinze service log rodando dentro de um service consumer group, ou seja, vários services new orders enviando, vários services logs escutando, vários services fraud detector detectando e vários services e-mail enviando e-mail.

Escalei com várias mensagens sendo enviadas e recebidas, podendo desligar e ligar o Kafka, aumentar o número de partições, mudar essas configurações, etc.




