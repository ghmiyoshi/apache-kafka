package br.com.alura.ecommerce;

import java.util.HashMap;

import org.apache.kafka.clients.consumer.ConsumerRecord;

public class EmailService {

	public static void main(String[] args) {
		EmailService emailService = new EmailService();
		
		try(KafkaService<EmailService> service = new KafkaService(EmailService.class.getSimpleName(), "ECOMMERCE_SEND_EMAIL", emailService::parse, String.class, new HashMap<>())) { // Uso method reference para executar essa função para cada record
			service.run();
		}
	}

	private void parse(ConsumerRecord<String, String> record) {
		System.out.println("*******************************************");
		System.out.println("Send email...");
		System.out.println("Key: " + record.key());
		System.out.println("Value: " + record.value());
		System.out.println("Partition: " + record.partition());
		System.out.println("Offset: " + record.offset());

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("Email sent!");
	}

}
