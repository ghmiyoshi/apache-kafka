package br.com.alura.ecommerce;

import java.util.HashMap;

import org.apache.kafka.clients.consumer.ConsumerRecord;

public class FraudDetectorService {

	public static void main(String[] args) {
		FraudDetectorService fraudDetectorService = new FraudDetectorService();
		
		// Tento executar esse código e se acontecer qualquer exception nesse bloco ele fecha o KafkaService
		try(KafkaService<Order> service = new KafkaService<>(FraudDetectorService.class.getSimpleName(), "ECOMMERCE_NEW_ORDER", fraudDetectorService::parse, Order.class, new HashMap<>())){
			service.run();
		}
	}

	private void parse(ConsumerRecord<String, Order> record) {
		System.out.println("*******************************************");
		System.out.println("Processing new order, checking for fraud...");
		System.out.println("Key: " + record.key());
		System.out.println("Value: " + record.value());
		System.out.println("Partition: " + record.partition());
		System.out.println("Offset: " + record.offset());

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("Order processed!");
	}

}
