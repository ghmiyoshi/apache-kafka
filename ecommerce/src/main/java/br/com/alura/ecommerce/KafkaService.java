package br.com.alura.ecommerce;

import java.io.Closeable;
import java.time.Duration;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.regex.Pattern;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

public class KafkaService<T> implements Closeable{

	private final KafkaConsumer<String, T> consumer;
	private final ConsumerFunction<T> parse;

	public KafkaService(String groupId, String topic, ConsumerFunction<T> parse, Class<T> type, Map<String, String> properties) {
		this(parse, groupId, type, properties);
		consumer.subscribe(Collections.singletonList(topic)); // Consumo uma mensagem, para isso faço o subscribe em algum topico passando uma lista
	}

	public KafkaService(String groupId, Pattern topic, ConsumerFunction<T> parse, Class<T> type, Map<String, String> properties) {
		this(parse, groupId, type, properties);
		consumer.subscribe(topic);
	}
	
	private KafkaService(ConsumerFunction<T> parse, String groupId, Class<T> type, Map<String, String> properties) {
		this.parse = parse;
		this.consumer = new KafkaConsumer<>(getProperties(type, groupId, properties));
	}

	public void run() {
		while(true) { // Deixo o serviço executando para escutar novas mensagens
			ConsumerRecords<String, T> records = consumer.poll(Duration.ofMillis(100)); // Pergunto por algum tempo se tem mensagem dentro desse topico

			if (!records.isEmpty()) {
				System.out.println("Found " + records.count() + " records.");

				records.forEach(record -> {
					parse.consume(record);
				});
			}
		}
	}

	private Properties getProperties(Class<T> type, String groupId, Map<String, String> overrideProperties) {
		Properties properties = new Properties();

		properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
		properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, GsonDeserializer.class.getName());
		properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
		properties.setProperty(ConsumerConfig.CLIENT_ID_CONFIG, UUID.randomUUID().toString());
		properties.setProperty(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "1");
		properties.setProperty(GsonDeserializer.TYPE_CONFIG, type.getName());
		properties.putAll(overrideProperties);
		
		/* ConsumerConfig.MAX_POLL_RECORDS_CONFIG - O máximo de records que eu quero consumir por vez, no máximo 1, eu quero consumir de um em um, me dá o próximo.
		 Configuro o máximo de records que quero, para que tenha mais oportunidades de não duplicar mensagens, de não executar duas vezes a mesma mensagem, porque falhou o commit. */
		
		return properties;
	}

	@Override
	public void close() {
		consumer.close();
	}

}
